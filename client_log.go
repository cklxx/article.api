package main

import (
	"fmt"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/article.api/models"
)

type Clog struct {
	Id         string `json:"id"`
	Event      string `json:"event"`
	Params     string `json:"params"`
	CreateTime string `db:"create_time" json:"create_time"`
	UpdateTime string `db:"update_time" json:"update_time"`
}

func logTracer(c *gin.Context) {
	event := c.PostForm("event")
	params := c.PostForm("params")

	currentTime := time.Now().Unix()
	q := models.Db.MustExec(`INSERT INTO client_log (event,params,create_time,update_time) VALUES ($1, $2, $3, $4)`,
		event, params, currentTime, currentTime)
	insertId, err := q.RowsAffected()
	if err != nil {
		fmt.Println(err)
	}

	c.JSON(200, gin.H{
		"status": 200,
		"id":     insertId,
	})
}
