package main

import (
	"crypto/md5"
	"fmt"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/article.api/models"
)

type User struct {
	Id         string `json:"id"`
	Name       string `json:"name"`
	Username   string `json:"username"`
	Password   string `json:"password"`
	Avatar     string `json:"avatar"`
	CreateTime string `db:"create_time" json:"create_time"`
	UpdateTime string `db:"update_time" json:"update_time"`
}

func registerUser(c *gin.Context) {
	tx := models.Db.MustBegin()
	username := c.PostForm("username")
	user := []User{}
	err := models.Db.Select(&user, "SELECT * FROM users where username = $1", username)
	if err == nil && len(user) != 0 {
		c.JSON(200, gin.H{
			"status":  300,
			"message": "新增用户失败,该用户名已被使用",
		})
	} else {
		name := c.PostForm("name")
		password := c.PostForm("password")
		avatar := c.PostForm("avatar")

		currentTime := time.Now().Unix()
		fmt.Println(name,
			username,
			password,
			avatar,
			currentTime,
			currentTime)
		tx.MustExec("INSERT INTO users (name,username,password,avatar,create_time,update_time) VALUES ( $1, $2, $3, $4, $5, $6 )",
			name,
			username,
			password,
			avatar,
			currentTime,
			currentTime,
		)
		err = tx.Commit()
		if err != nil {
			c.JSON(200, gin.H{
				"status":  300,
				"message": "新增用户失败",
			})
		} else {
			handleCookie(c, User{
				Name:       name,
				Username:   username,
				Password:   password,
				Avatar:     avatar,
				CreateTime: fmt.Sprint(currentTime),
				UpdateTime: fmt.Sprint(currentTime),
			})
		}
	}
}

func login(c *gin.Context) {
	token := c.PostForm("token")
	username := c.PostForm("username")

	if !checkCookieInRds(username, token) {
		password := c.PostForm("password")
		fmt.Println(username, password)
		user := []User{}
		err := models.Db.Select(&user, "SELECT * FROM users where username = $1 and password = $2", username, password)
		if err != nil || len(user) == 0 {
			c.JSON(200, gin.H{
				"status": 200,
				"data":   "获取失败 你的参数 username:" + username,
				"err":    err,
			})
		} else {
			handleCookie(c, user[0])
		}
	} else {
		c.JSON(200, gin.H{
			"status": 200,
			"data":   "没有超出cookie的缓存时间，登陆成功",
		})
	}
}

func handleCookie(c *gin.Context, user User) {
	token := md5fromStr(user.Username + ":" + user.Password)
	err := models.Rdb.Set(models.Ctx, user.Username, token, time.Second*36000).Err()
	if err != nil {
		c.JSON(200, gin.H{
			"status": 500,
			"data":   user.Username,
			"err":    err,
		})
	} else {
		c.JSON(200, gin.H{
			"status":    200,
			"data":      user.Username,
			"token":     token,
			"user_name": user.Username,
		})
	}
}

func checkCookieInRds(user string, cookie string) bool {
	cookieInRds, err := models.Rdb.Get(models.Ctx, user).Result()
	if err != nil {
		return false
	}
	return cookieInRds == cookie
}

func md5fromStr(str string) string {
	data := []byte(str)
	has := md5.Sum(data)
	md5str1 := fmt.Sprintf("%x", has)

	return md5str1
}
