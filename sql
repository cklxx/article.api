INSERT INTO articles (title,owner,description,content,create_time,update_time,is_blog,out_url,tag,src_en,src_cn,cover_image) 
VALUES ( '第一篇文章', 'ckl',  '描述',  '# 原来如厕',  '0425',  '0425', true,  '',  'blog', 'in-site', '站内', '' );

drop table articles;
CREATE TABLE articles(
    id varchar(64),
    idx serial,
    title text,
    description text,
    content text,
    owner text,
    create_time character varying,
    update_time character varying,
    is_blog boolean,
    out_url text,
    tag character varying,
    src_en text,
    src_cn text,
    cover_image text,
    PRIMARY KEY(id)
);

CREATE TABLE client_log(
    id SERIAL,
    event text,
    params text,
    create_time character varying,
    update_time character varying,
    PRIMARY KEY(id)
);

CREATE TABLE  users(
    id SERIAL,
    name text,
    username text,
    password text,
    avatar text,
    create_time character varying,
    update_time character varying,
    PRIMARY KEY(id)
);