package main

import (
	"os"

	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
	"gitlab.com/article.api/models"
)

func main() {
	if os.Getenv("GIN_MODE") == "release" {
		gin.SetMode(gin.ReleaseMode)
	}

	r := gin.New()
	r.SetTrustedProxies([]string{"192.168.1.2"})
	models.InitDB()
	models.InitRDB()

	r.GET("/api/feed", feedPacker)
	r.GET("/api/search", searchPacker)
	r.GET("/api/suggest", suggestion)
	r.GET("/api/article", articlePacker)
	r.POST("/api/article", articleSeter)
	r.POST("/api/article/update", articleUpdate)

	r.POST("/api/log", logTracer)
	r.POST("/api/classify", daprClientSend)
	r.POST("/api/user/res", registerUser)
	r.POST("/api/user/login", login)
	r.Run("0.0.0.0:4000")
}
