package models

import (
	"context"
	"log"

	"github.com/expectedsh/go-sonic/sonic"
	"github.com/go-redis/redis/v8"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

const Pswd = "cklyzy:1216"
const SonicHost = "localhost"
const SonicPost = 1491

var Db *sqlx.DB
var Rdb *redis.Client
var Ctx = context.Background()
var SonicIngester sonic.Ingestable
var SonicSearch sonic.Searchable

func InitDB() {
	if Db != nil {
		return
	}
	var err error
	Db, err = sqlx.Connect("postgres", "user=postgres password=cklyzy001 dbname=postgres sslmode=disable")
	if err != nil {
		log.Fatalln(err)
	}
	log.Println("[db]:init postgres db sucess")
	// defer Db.Close()
}

func InitRDB() {

	if Rdb != nil {
		return
	}
	Rdb = redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})
	log.Println("[db]:init redis db sucess")
}

func InitSonic() {
	if SonicIngester == nil {
		var err error

		SonicIngester, err = sonic.NewIngester("localhost", 1491, Pswd)
		if err != nil {
			panic(err)
		}
		log.Println("[db]:init SonicIngester sucess")
	}

}

func QuitSonicSearch() {
	if SonicIngester != nil {
		err := SonicIngester.Quit()
		if err != nil {
			log.Println(err)
		}
		log.Println("[db]:Quit SonicIngester sucess")
	}
	if SonicSearch != nil {
		err := SonicSearch.Quit()
		if err != nil {
			log.Println(err)
		}
		log.Println("[db]:Quit SonicSearch sucess")
	}
}

func SonicConfig() (string, int, string) {
	return SonicHost,
		SonicPost,
		Pswd
}
