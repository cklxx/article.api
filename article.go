package main

import (
	"fmt"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/article.api/models"
)

type Article struct {
	Id          string `json:"id"`
	Idx         string `json:"idx"`
	Title       string `json:"title"`
	Owner       string `json:"owner"`
	Description string `json:"description"`
	Content     string `json:"content"`
	CreateTime  string `db:"create_time" json:"create_time"`
	UpdateTime  string `db:"update_time" json:"update_time"`
	IsBlog      bool   `db:"is_blog" json:"is_blog"`
	OutUrl      string `db:"out_url" json:"out_url"`
	Tag         string `json:"tag"`
	SrcEn       string `db:"src_en" json:"src_en"`
	SrcCn       string `db:"src_cn" json:"src_cn"`
	CoverImage  string `db:"cover_image" json:"cover_image"`
}

func feedPacker(c *gin.Context) {
	offset := c.DefaultQuery("offset", "0")
	count := c.DefaultQuery("count", "10")

	articles := []Article{}
	err := models.Db.Select(&articles, "SELECT * FROM articles order by update_time desc limit $1 offset $2", count, offset)
	if err != nil {
		fmt.Println(err)
	}

	c.JSON(200, gin.H{
		"status": 200,
		"data":   articles,
	})
}

func articlePacker(c *gin.Context) {
	id := c.Query("id")

	articles := []Article{}
	err := models.Db.Select(&articles, "SELECT * FROM articles where id = $1", id)
	if err != nil || len(articles) == 0 {
		fmt.Println(err)
		c.JSON(200, gin.H{
			"status": 200,
			"data":   "获取失败 你的参数 id：" + id,
		})
	} else {
		c.JSON(200, gin.H{
			"status": 200,
			"data":   articles[0],
		})
	}
}

func articleSeter(c *gin.Context) {
	tx := models.Db.MustBegin()

	title := c.PostForm("title")
	owner := c.PostForm("owner")
	description := c.PostForm("description")
	content := c.PostForm("content")
	pureContent := c.PostForm("pure_content")
	isBlog := c.PostForm("is_blog") == "true"
	outUrl := c.PostForm("out_url")
	tag := c.PostForm("tag")
	srcEn := c.PostForm("src_en")
	srcCn := c.PostForm("src_cn")
	coverImage := c.PostForm("cover_image")
	idstr := c.PostForm("id")

	go insertSearch(idstr, BucketTitle, title)
	go insertSearch(idstr, BucketContent, pureContent)

	currentTime := time.Now().Unix()
	tx.MustExec(
		`INSERT INTO articles
		(id,title,owner,description,content,create_time,update_time,is_blog,out_url,tag,src_en,src_cn,cover_image) 
		VALUES 
		( $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12,$13 )`,
		idstr,
		title,
		owner,
		description,
		content,
		currentTime,
		currentTime,
		isBlog,
		outUrl,
		tag,
		srcEn,
		srcCn,
		coverImage,
	)
	// Named queries can use structs, so if you have an existing struct (i.e. person := &Person{}) that you have populated, you can pass it in as &person
	err := tx.Commit()
	if err != nil {
		c.JSON(200, gin.H{
			"status":  200,
			"message": "新增文章失败",
		})
	} else {
		c.JSON(200, gin.H{
			"status":  200,
			"message": "新增文章成功",
		})
	}
}

func articleUpdate(c *gin.Context) {
	tx := models.Db.MustBegin()

	title := c.PostForm("title")
	owner := c.PostForm("owner")
	description := c.PostForm("description")
	content := c.PostForm("content")
	isBlog := c.PostForm("is_blog") == "true"
	outUrl := c.PostForm("out_url")
	tag := c.PostForm("tag")
	srcEn := c.PostForm("src_en")
	srcCn := c.PostForm("src_cn")
	coverImage := c.PostForm("cover_image")
	idstr := c.PostForm("id")

	go insertSearch(idstr, BucketTitle, title)
	go insertSearch(idstr, BucketContent, content)

	currentTime := time.Now().Unix()
	tx.MustExec(
		`UPDATE articles
		SET id = $1,title = $2,owner =$3,description =$4,content =$5,update_time = $6,is_blog = $7,out_url = $8,tag = $9, src_en = $10, src_cn = $11,cover_image= $12
		WHERE id = $1`,
		idstr,
		title,
		owner,
		description,
		content,
		currentTime,
		isBlog,
		outUrl,
		tag,
		srcEn,
		srcCn,
		coverImage,
	)
	// Named queries can use structs, so if you have an existing struct (i.e. person := &Person{}) that you have populated, you can pass it in as &person
	err := tx.Commit()
	if err != nil {
		c.JSON(200, gin.H{
			"status":  200,
			"message": "更新文章失败",
		})
	} else {
		c.JSON(200, gin.H{
			"status":  200,
			"message": "更新文章成功",
		})
	}
}
