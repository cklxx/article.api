package main

import (
	"log"
	"strings"

	"github.com/expectedsh/go-sonic/sonic"
	"github.com/gin-gonic/gin"
	"gitlab.com/article.api/models"
)

const (
	SonicCollection string = "article"
	BucketTitle     string = "title"
	BucketContent   string = "content"
)

func searchPacker(c *gin.Context) {

	keyword := c.DefaultQuery("keyword", "0")
	if keyword == "" {
		c.JSON(200, gin.H{
			"status": 200,
			"data":   "keyword 为空",
		})
		return
	}
	SonicHost, SonicPost, Pswd := models.SonicConfig()

	SonicSearch, err := sonic.NewSearch(SonicHost, SonicPost, Pswd)
	if err != nil {
		c.JSON(200, gin.H{
			"status": 200,
			"data":   err,
		})
		return
	}
	suggestQuery, err := SonicSearch.Suggest(SonicCollection, BucketTitle, keyword, 10)
	if err != nil {
		log.Println("Suggest", err)
	}
	realQuery := keyword
	if len(suggestQuery) != 0 {
		realQuery = suggestQuery[0]
	}

	resultsTitle, err := SonicSearch.Query(SonicCollection, BucketTitle, realQuery, 10, 0, sonic.LangCmn)
	if err != nil {
		log.Println(err)
		resultsTitle = []string{}
	}
	resultsContent, err := SonicSearch.Query(SonicCollection, BucketContent, realQuery, 10, 0, sonic.LangCmn)
	if err != nil {
		log.Println(err)
		resultsContent = []string{}
	}

	SonicSearch.Quit()

	articleResults := []Article{}
	qlTemplate := "('" + strings.Join(resultsTitle[:], "','") + strings.Join(resultsContent[:], "','") + "')"
	err = models.Db.Select(&articleResults, "SELECT * FROM articles where id in "+qlTemplate)

	if err != nil || len(articleResults) == 0 {
		c.JSON(200, gin.H{
			"status": 200,
			"data":   "获取失败，没找到你需要内容",
		})
	} else {
		c.JSON(200, gin.H{
			"status": 200,
			"data":   articleResults,
		})
	}
}

func insertSearch(key string, bucket string, text string) []sonic.IngestBulkError {
	SonicHost, SonicPost, Pswd := models.SonicConfig()
	SonicIngester, err := sonic.NewIngester(SonicHost, SonicPost, Pswd)
	if err != nil {
		log.Println(err)
	}
	erra := SonicIngester.BulkPush(SonicCollection, bucket, 3, []sonic.IngestBulkRecord{
		{Object: key, Text: text},
	}, sonic.LangNone)
	if erra != nil {
		erra = SonicIngester.BulkPush(SonicCollection, bucket, 3, []sonic.IngestBulkRecord{
			{Object: key, Text: text},
		}, sonic.LangNone)
	}
	SonicIngester.Quit()
	return erra
}

// 使用fst算法猜测输入的词
func suggestion(c *gin.Context) {
	keyword := c.DefaultQuery("keyword", "0")
	if keyword == "" {
		c.JSON(200, gin.H{
			"status": 200,
			"data":   "keyword 为空",
		})
		return
	}
	SonicHost, SonicPost, Pswd := models.SonicConfig()

	SonicSearch, err := sonic.NewSearch(SonicHost, SonicPost, Pswd)
	if err != nil {
		c.JSON(200, gin.H{
			"status": 200,
			"data":   err,
		})
		return
	}

	results, err := SonicSearch.Suggest(SonicCollection, BucketTitle, keyword, 10)
	if err != nil {
		log.Println(err)
		results = []string{}
	}
	SonicSearch.Quit()

	c.JSON(200, gin.H{
		"status": 200,
		"data":   results,
	})
}
