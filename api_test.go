package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"testing"

	"github.com/expectedsh/go-sonic/sonic"
	"github.com/go-redis/redis/v8"
	"gitlab.com/article.api/models"
)

type Persion struct {
	Name string
	Test string
}

var hostUrl = "http://localhost:4000"

func TestApiArticle(t *testing.T) {
	resp, err := http.Get(hostUrl + "/api/article?id=3")
	if err != nil {
		t.Fail()
	}
	defer resp.Body.Close()
	s, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fail()
	}
	t.Log(string(s))
}

func TestApiArticlePost(t *testing.T) {

	s := "id=3343434&title=test&owner=test&description=test&content=testsss&is_blog=test&out_url=test&tag=test&src_en=tests&src_cn=test&cover_image="
	t.Log(s)
	resp, err := http.Post(hostUrl+"/api/article", "application/x-www-form-urlencoded", strings.NewReader(s))
	// sf
	if err != nil {
		t.Fail()
	}
	defer resp.Body.Close()
	//io.Reader
	body, _ := ioutil.ReadAll(resp.Body)
	fmt.Println(string(body))
	t.Log(string(body))
}

func TestApiFeed(t *testing.T) {
	resp, err := http.Get(hostUrl + "/api/feed?offset=0&count=10")
	if err != nil {
		t.Fail()
	}

	defer resp.Body.Close()

	s, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fail()
	}
	t.Log(string(s))
}

func TestApiClogPost(t *testing.T) {

	s := `event=test&params={"test":"ddd"}`
	t.Log(s)
	resp, err := http.Post(hostUrl+"/api/log", "application/x-www-form-urlencoded", strings.NewReader(s))
	if err != nil {
		t.Fail()
	}
	defer resp.Body.Close()
	//io.Reader
	body, _ := ioutil.ReadAll(resp.Body)
	fmt.Println(string(body))
	t.Log(string(body))
}

func TestApiClassifyPost(t *testing.T) {

	file, err := os.Open("image/cat.jpeg")

	if err != nil {
		t.Log(err)
	}
	resp, err := http.Post(hostUrl+"/api/classify", "binary/octet-stream", file)
	fmt.Println(resp)
	if err != nil {
		t.Log(err)
	}
	defer file.Close()

	defer resp.Body.Close()
	//io.Reader
	body, _ := ioutil.ReadAll(resp.Body)
	fmt.Println(string(body))
	t.Log(string(body))
}

func TestApiRds(t *testing.T) {
	models.InitRDB()
	Rdb := models.Rdb
	ctx := models.Ctx
	err := Rdb.Set(ctx, "key", "value", 0).Err()
	if err != nil {
		panic(err)
	}

	val, err := Rdb.Get(ctx, "key").Result()
	t.Log(val)

	if err != nil {
		panic(err)
	}
	fmt.Println("key", val)

	val2, err := Rdb.Get(ctx, "key2").Result()

	if err == redis.Nil {
		fmt.Println("key2 does not exist")
	} else if err != nil {
		panic(err)
	} else {
		t.Log("val2", val2)
	}
}

func TestApiSearch(t *testing.T) {
	models.InitSonic()

	results, err := models.SonicSearch.Query("article", "general", "成书", 10, 0, sonic.LangNone)

	if err != nil {
		panic(err)
	} else {
		t.Log("results", results)
	}
}

func TestApiSonic(t *testing.T) {
	models.InitSonic()

	err := models.SonicIngester.FlushBucket("article", "general")

	if err != nil {
		panic(err)
	} else {
		t.Log("results suceess")
	}
}

func TestApiSearcha(t *testing.T) {
	resp, err := http.Get(hostUrl + "/api/search?keyword=互联网")

	if err != nil {
		t.Fail()
	}
	defer resp.Body.Close()
	s, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fail()
	}
	t.Log(string(s))
}
